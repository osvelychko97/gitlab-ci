package com.gitlab.train.gitlabci.service;

import com.gitlab.train.gitlabci.domain.UserData;

public interface StringRevert {

    UserData revertString(UserData data);

}
