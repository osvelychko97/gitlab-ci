package com.gitlab.train.gitlabci.service.impl;

import com.gitlab.train.gitlabci.domain.UserData;
import com.gitlab.train.gitlabci.service.StringRevert;
import org.springframework.stereotype.Service;

@Service
public class StringRevertImpl implements StringRevert {

    @Override
    public UserData revertString(UserData data) {
        var revertString = new StringBuilder();
        char[] chars = data.getUserString().toCharArray();

        for (var i = data.getUserString().length()-1; i >= 0; i--) {
            revertString.append(chars[i]);
        }
        data.setUserString(revertString.toString());

        return data;
    }
}
