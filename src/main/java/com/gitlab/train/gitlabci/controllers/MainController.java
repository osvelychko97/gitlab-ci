package com.gitlab.train.gitlabci.controllers;

import com.gitlab.train.gitlabci.domain.UserData;
import com.gitlab.train.gitlabci.service.StringRevert;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@AllArgsConstructor
public class MainController {

    @Autowired
    private StringRevert stringRevert;

    @PostMapping("/api/v1/revert")
    public ResponseEntity<UserData> getString(@RequestBody UserData data) {

        return new ResponseEntity<>(stringRevert.revertString(data), HttpStatus.OK);
    }
}
