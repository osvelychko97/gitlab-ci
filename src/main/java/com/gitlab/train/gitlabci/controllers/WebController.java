package com.gitlab.train.gitlabci.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class WebController {
    private static final String INDEX = "index";

    @GetMapping(value = "/")
    public String greeting() {

        return INDEX;
    }
}
