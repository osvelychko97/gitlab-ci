package com.gitlab.train.gitlabci.service.impl;

import com.gitlab.train.gitlabci.domain.UserData;
import com.gitlab.train.gitlabci.service.StringRevert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class StringRevertImplTest {

    private StringRevert stringRevert;

    @Before
    public void init() {
        stringRevert = new StringRevertImpl();
    }

    @Test
    public void shouldReturnRightRevertString() {
        var actual = stringRevert.revertString(new UserData("qwerty")).getUserString();

        assertEquals("ytrewq", actual);
    }

    @Ignore
    @Test
    public void shouldFail() {
        fail();
    }
}